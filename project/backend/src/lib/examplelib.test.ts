import { addNumbers } from "./examplelib"


describe('My Lib', () => {

  test('should add numbers', () => {
    expect(addNumbers(1, 2)).toEqual(3)
    expect(addNumbers(0, 2)).toEqual(2)
    expect(addNumbers(2, 2)).toEqual(4)
  })

})


