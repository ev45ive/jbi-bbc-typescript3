export interface Book {
  id: string;
}
;

export type FindBooksCriteria = { title: string; } |
{ author: string; };

export interface FindBooksResults {
  items: Book[];
}


export interface CreateBookDTO {
  title: string
  authors: string[]
}

// json schma
// function validateSchema<T>(data:any, schema: Schema<T> ): dto is T{
//   return schema.validate(date)? true :false
// }

export function isValidCreateBookDTO(dto: any): dto is CreateBookDTO {
  if (!('title' in dto)) { return false }
  if (!('authors' in dto) || !(dto.authors instanceof Array)) { return false }
  return true
}