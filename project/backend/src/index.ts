
import express from 'express'
import { configureApp } from './app'
import session from 'express-session'
const host = process.env.HOST || 'localhost'
const port = Number(process.env.PORT || '8080')

export const app = express();

app.use(express.json())
app.use(session({
  secret: 'top secret cat',
  cookie: {
    secure: false
  },
  resave: true,
  saveUninitialized: true,
}))

configureApp(app)

app.listen(port, host, () => {
  console.log(`Listening on http://${host}:${port}/`);
})


// import { awesome } from 'express-session'
// awesome(123)

declare module 'express-session' {
  function awesome(x: 123): void

  interface SessionData {
    views: number;
  }
}

declare global {
  namespace Express {
    // These open interfaces may be extended in an application-specific manner via declaration merging.
    // See for example method-override.d.ts (https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/method-override/index.d.ts)
    interface Request {
      secret: '123'
    }
    interface Response { }
    interface Application { }
  }
}