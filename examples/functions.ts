function add(x: number, y: number) {

  if (x == 42) { return 'Universe and stuff' }

  return x + y;
}

let myAdd = function (x: number, y: number): number {
  return x + y;
};


// add(12, 123).toExponential(2) // Error

type AddFuncType = (baseValue: number, increment: number) => number;

// let myAdd2: (baseValue: number, increment: number) => number = function (x: number, y: number): number {
// let myAdd2: (baseValue: number, increment: number) => number = function (x, y) {

let myAdd2: AddFuncType = function (x, y) { return x + y; };

/* Function interface */

interface AddFuncType2 {
  (x: string, y: string): string
}

let myAdd3: AddFuncType2 = function (x, y) {
  return x + y;
};
const result = myAdd3('123', '123')


/* Function and object interface */

interface AddFuncType3 {
  (x: string, y: string): string
  someStaticProp?: string;
  // [x: string]: string;
}

let myAdd4: AddFuncType3 = function (x, y) {
  return x + y;
};
myAdd4.someStaticProp = 'smth'
const result4 = myAdd4('123', '123')


/* Overloads */

interface AddFuncType4 {
  (x: string, y: string): string
  (x: number, y: number): number
}

let myAdd5: AddFuncType4 = function (x, y) {
  if (typeof x == 'string' && typeof y == 'string') {
    return x + y;
  }
  if (typeof x == 'number' && typeof y == 'number') {
    return x + y;
  }
  return x + y;
};
const result1 = myAdd5(123, 123)
const result2 = myAdd5('123', '123')

/* Constructors */

// let Thing = (function () {
//   function Thing() { }
//   Thing.prototype = {}
//   return Thing
// })()
class Thing { base: string }
class SpecialThing extends Thing { extras: string }

interface ThingMaker {
  (ctr: new () => Thing): Thing
}

// const makeAThing: ThingMaker = function (ctr: new () => Thing) {
const makeAThing: ThingMaker = function (ctr) {
  return new Thing()
}

const thing = makeAThing(Thing)
thing.base

// TODO: Fix with Generics!
const spacialthing = makeAThing(SpecialThing)
spacialthing.base;
(spacialthing as SpecialThing).extras

/* Overloads 2 */


// interface SearchFunc {
//   (source: string, searchString: string): boolean
//   (source: string, searchString: string, position: number): boolean
// }
// const mySearch: SearchFunc = (source) => { // OK - igore extra params

/* Overloads with nice Documentation / Hints */

/**
 * Search string
 * @param source 
 * @param searchString 
 */
function mySearch2(source: string, searchString: string): boolean
/**
 * Search string starting from position
 * Search string starting from position
 * Search string starting from position
 * @see http://stackoverflow.com
 * @param source 
 * @param searchString 
 * @param position 
 */
function mySearch2(source: string, searchString: string, position: number): boolean
function mySearch2(source, needle: string, position?: number) {

  return source.includes(needle, position)
}
mySearch2('batman', 'bat', 1)

// tsc --declaration examples/functions.ts

// mySearch2(source: string, searchString: string, position: number): boolean
// 2/2
// Search string starting from position
// mySearch2()

/* Initial, optional, spread */


function sum(initialValue = 0, ...rest: number[]): number {
  return rest.reduce((sum, x) => sum + x, initialValue)
}

const sum1 = sum() // 0
const sum2 = sum(10) // 0
const sum4 = sum(10, 11, 12, 13) // 0
const numbrs = [11, 12, 13]

const sum5 = sum(10, ...numbrs) // 0

/* Tuple as parameters type */

type Args = [id: number, name: string]

function someFn(...args: Args) { }

/* This context */

interface Elem {
  type: 'button',
  onclick?: (this: Elem, event: Event) => void
}

const button: Elem = { type: 'button' }

button.onclick = function (event) {
  this.type == 'button'
}

/* Overloads 3 */
interface User { }
interface Admin { }
interface Moderator { }
type Character = User | Admin | Moderator

function create(name: "User"): User;
/**
 * Admin
 * @param name {"Admin"}
 */
function create(name: "Admin"): Admin;
function create(name: "Moderator"): Moderator;
function create(name: "User" | "Admin" | "Moderator"): Character {
  switch (name) {
    case 'User': return {} as User
    case 'Admin': return {} as Admin
    case 'Moderator': return {} as Moderator
  }
}
const m = create("Moderator"); // m is a Moderator
const a = create('Admin'); // a is a Admin