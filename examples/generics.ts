

/* Container / Container wrapper */
const y: string[] = ["a", "b", "c"]
const x: Array<string> = ["a", "b", "c"]
const z: Array<string | number> = ["a", 123, "c"]

const arr1: Array<string> = ['string']
const arr2: Array<string | number> = ['string', 123]
const arr3: Array<Array<string>> = [
  ['X', 'O', ' '],
  ['O', 'X', ' '],
  [' ', 'O', 'O'],
]

/* Container wrapper */
type Ref<T> = {
  current: T;
  // previous: T;
};

const ref1: Ref<number> = { current: 123 };
const ref2: Ref<string> = { current: "aaa" };

function getValue<T>(ref: Ref<T>): T {
  return ref.current;
}

/* Collection wrapper */
export interface GetResponse<K, T> {
  kind: K;
  totalItems: number;
  items: T[];
}

/* Class generic */

/** A class definition with a generic parameter */
/* LIFO - LastIn FirstOut | FIFO - FirstIn FirstOut */

class Queue<T> {
  private data: T[] = [];
  push(item: T) { this.data.push(item); }
  pop(): T | undefined { return this.data.shift(); }
}

/* useless generics: */

function parse<T>(name: string): T { return {} as T }
function serialize<T>(obj: T): string { return '' }

/* same as: */

function parse2(name: string) { return {} as Object }
function serialize2(obj: any): string { return '' }

/* usefull generics: */

function identity<T>(x: T): T { return x; }

function takeFirst<T>(arr: T[]): T { return arr[0] }

const A1: string = takeFirst<string>(['123', '123']);
const A2: string = takeFirst(['123', '123']);
const A3 = takeFirst([123]); // number
const A4 = takeFirst([123, '123']); // number
const A5: boolean = takeFirst([/* intelisense tells : boolean! */])


interface IContructThe<T> {
  // (...args: any[]): T;
  new(...args: any[]): T;
}

class Abc {
  constructor() {
    return { value: 'pancakes' }
  }
}

// const setItemFactory = (factory: Abc) => {
const setItemFactory = (factory: IContructThe<Abc>) => {
  new factory(123)
}
setItemFactory(Abc)

/* Intance type vs Class Type */

class Person {
  constructor(public name: string = '') { }
  static getLegalAge() { }
}
class Employee extends Person { }

Person.getLegalAge()

const alice = new Person('Alice')
alice.name

const client: Person = alice
const clientFactory: IContructThe<Person> = Employee

const clientFactory2: typeof Person = Employee

type PersonConstructor = typeof Person
const clientFactory3: PersonConstructor = Employee

/* Generic constraints - extends  */

// function printName<T extends { name: string }>(arg: T) { }

type ObjWithAtLeastAName = { name: string };
function printName<T extends ObjWithAtLeastAName>(arg: T) { }

printName({ name: "Kate" }); // OK
printName({ name: "Michael", age: 22 }); // OK
// printName({ age: 22 }); // Error!

type ObjWithNameOrObjecttWithAge = { name: string } | { age: number };
function printName2<T extends ObjWithNameOrObjecttWithAge>(arg: T) { }
printName2({ name: "BOB" }); // OK!
printName2({ age: 22 }); // OK!


/* Constraints betteen multiple Generic params */

function makePair<T, U>(arg1: T, arg2: U): [T, U] {
  return [arg1, arg2]  // as const
}
const pair = makePair(123, 'user name')


function defaults<T extends object, U extends T>(obj1: T, obj2: U) {
  return { ...obj1, ...obj2 };
}
defaults({ a: 0, b: 1 }, { a: 123, b: 123 });
defaults({ a: 0, b: 1 }, { a: 123, b: 123, c: 234 });
// defaults({ a: 0, b: 1 }, { a: 123, c: 234 }); // Error - missing 'b' from T


/* SomeType[somePart], 'x' in object, keyof SomeType */

enum Role {
  admin = 'ADMIN_ROLE',
  user = 'USER_ROLE',
  moderator = 'MODERATOR_ROLE',
}
// type roles = "admin" | "user" | "moderator"
type RoleNames = keyof typeof Role

// function getRole<TRole extends typeof Role, UName extends keyof typeof Role>(roles: typeof Role, userRole: UName) {
function getRole<TRole extends object, UName extends keyof TRole>(roles: TRole, userRole: UName) {
  return roles[userRole]
}

const role = getRole(Role, 'moderator')
const role2 = getRole({ test: 'TEST_ROLE' as const }, 'test')
const role3 = getRole({ ...Role, extra: 'EXTRA_ROLE' }, 'extra')


interface User { id: string, role: Role }
interface Admin /*extends User*/ { id: string, role: Role.admin }
interface Moderator /*extends User*/ { id: string, role: Role.admin }

function getUserRole<T extends User>(user: T): T['role'] {
  return user.role
}
const u1 = getUserRole({ id: '123', role: Role.admin })


/* Variance - covariant, contravriant, bivariant, invariant */

class Animal { constructor(public name: string) { } }
class Tiger extends Animal {
  constructor(public name: string, public color: string) { super(name) }
}

class Zoo<Animal> {
  animals: Animal[] = []
}

const zoo = new Zoo()
zoo.animals.push(new Animal('Dog'))
zoo.animals.push(new Tiger('Cat', 'white'))
const animal = zoo.animals.pop() // Animal

class CatZoo<T extends Animal> extends Zoo<T> {
  // animals: Animal[] = []
  animals: T[] = []
}
const catzoo = new CatZoo<Tiger>()
// catzoo.animals.push(new Animal('Dog'))
catzoo.animals.push(new Tiger('Cat', 'white'))
const animal2 = catzoo.animals.pop() // Tiger
animal2?.color // ???

/* Pattern matching on instance type */

class Character { name!: string; }
class User extends Character { age!: number; }
class Enemy extends Character { hp!: number; }

type CharacterType = User | Enemy
function attack(ch: Character) {
  // function attack(ch: CharacterType) {
  if (ch instanceof User) {
    ch.age; // OK
    ch.name; // OK
  } else if (ch instanceof Enemy) {
    ch.hp; // OK
    ch.name; // OK
  } else {
    ch.name; // OK
  }
}




export interface VolumeInfo {
  title: string;
  subtitle: string;
  authors: string[];
  publisher: string;
  publishedDate: Date;
  description: string;
  pageCount: number;
  printType?: string;
  categories: string[];
  maturityRating?: string;
  allowAnonLogging?: boolean;
  contentVersion: string;
  imageLinks: string[];
  language: string;
  previewLink: string;
  infoLink: string;
  canonicalVolumeLink: string;
}

type title = VolumeInfo['title']
type VolumeKeys = keyof VolumeInfo
const key: VolumeKeys = 'title'

/* Type mappings */

type Dictionary = { [key: string]: string }

type PartialObject = { [key in 'id' | 'name' | 'images']: string }

type VolumeStrings = { [key in VolumeKeys]: string }

// type PartialVolume2 = { [k in 'id' | 'name' | 'images']?: VolumeInfo[k] } // Error
type PartialVolume = { [k in VolumeKeys]?: VolumeInfo[k] }
type RequiredVolume = { [k in VolumeKeys]-?: VolumeInfo[k] }

// type PartialVolume = { [k in VolumeKeys]?: VolumeInfo[k] }
// type PartialBook = { [k in VolumeKeys]?: VolumeInfo[k] }
// type PartialAlbum = { [k in VolumeKeys]?: VolumeInfo[k] }
// type PartialMovie = { [k in VolumeKeys]?: VolumeInfo[k] }

/* Generic Type Mapping */

// type PartialVolumeInfo = { [k in keyof VolumeInfo]?: VolumeInfo[k] }

// Predefined in : \node_modules\typescript\lib\lib.es5.d.ts
// type Partial<T> = { [k in keyof T]?: T[k] }

type PartialVolumeInfo = Partial<VolumeInfo>

const mock: PartialVolumeInfo = {
  title: '', authors: [], publishedDate: new Date()
}


type PickKeys<T extends {}, Keys extends keyof T = keyof T> = {
  [k in Keys]: T[k]
}

type AllKeys = PickKeys<VolumeInfo>
type SomeKeys = PickKeys<VolumeInfo, 'title' | 'authors'>
const some: SomeKeys = {
  title: '123', authors: []
}
const some2: Pick<VolumeInfo, 'title' | 'categories'> = {
  categories: [], title: ''
}

/* Conditional types */
// type R<T, U> = T extends U ? X : Y;

// Simple usage example:
type IsBoolean<T> = T extends boolean ? true : false;

type t01 = IsBoolean<number>; // false
type t02 = IsBoolean<string>; // false
type t03 = IsBoolean<true>; // true


type NonNullable<T> = T extends null | undefined ? never : T;

type t04 = NonNullable<number>; // number
type t05 = NonNullable<string | null>; // string
type t06 = NonNullable<string | number | null>; // string
type t07 = NonNullable<null | undefined>; // never

type StringsOnly<T> = T extends string ? T : never;

type Result = StringsOnly<"abc" | 123 | "ghi">;
// "abc" | never | "ghi", therefore only "abc" | "ghi"

type x = { x: 1, y: '2' }['x' | 'y']

type X<T = { x: 1, y: '2' }> = {
  [key in keyof T]: key
}[keyof T]

const x2: X = 'y'

type StringKeys<T> = {
  [key in keyof T]: T[key] extends string ? key : never
}[keyof T]


const obj2: StringKeys<VolumeInfo> = 'title'
// const obj3: StringKeys<VolumeInfo> = 'author' // error - not a string

// const obj: StringKeys<VolumeInfo> = {
//   authors: [] // never!
// }

type ObjectWithStringKeys<T> = {
  [key in StringKeys<T>]: T[key]
}

const result2: Partial<ObjectWithStringKeys<VolumeInfo>> = {
  title: '',
  // authors: [] // Error
}

type SomeUnknownGenericType = {
  x: string
}

class Test<T extends { x: string }> {
  x = 1
  method(): T {
    return { x: '' } as T
  }
}

// const { x: extracted } = { x: 123 }
// extracted == 123

// --------------------------------------- v THIS -----------------------> v Inferred => to this : never
// type TestMetodReturn<T extends { x: number, method(): any }> = T extends { method(): infer P } ? P : any;
type TestMetodReturn<T extends { x: number, method(): any }> = T extends { method(): infer P } ? P : never;

type Result2 = TestMetodReturn<Test<SomeUnknownGenericType>>