"use strict";
// let x: MAGIC_NUMBERS = 4 // error
var HTTP_CODES;
(function (HTTP_CODES) {
    HTTP_CODES[HTTP_CODES["OK"] = 200] = "OK";
    HTTP_CODES[HTTP_CODES["BAD_REQUEST"] = 400] = "BAD_REQUEST";
    HTTP_CODES[HTTP_CODES["UNAUTHORIZED"] = 401] = "UNAUTHORIZED";
    HTTP_CODES[HTTP_CODES["FORBIDDEN"] = 403] = "FORBIDDEN";
    HTTP_CODES[HTTP_CODES["I_AM_A_TEAPOT"] = 404] = "I_AM_A_TEAPOT";
})(HTTP_CODES || (HTTP_CODES = {}));
var InvoiceStatus;
(function (InvoiceStatus) {
    InvoiceStatus["SUBMITTED"] = "SUBMITTED";
    InvoiceStatus["APPROVED"] = "APPROVED";
    InvoiceStatus["PAID"] = "PAID";
    InvoiceStatus[InvoiceStatus["LEASED"] = 42] = "LEASED";
    InvoiceStatus[InvoiceStatus["UNLEASED"] = 43] = "UNLEASED"; // 43
})(InvoiceStatus || (InvoiceStatus = {}));
debugger;
var result = InvoiceStatus.APPROVED;
console.log(InvoiceStatus[result]);
function getStatusLabel(status) {
    switch (status) {
        case InvoiceStatus.SUBMITTED:
            return "invoice was submitted";
        case InvoiceStatus.APPROVED:
            return "it's approved";
        case InvoiceStatus.PAID:
            return "invoice paid";
    }
}
